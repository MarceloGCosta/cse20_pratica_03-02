/**
 * @author Marcelo G. Costa <marcelo@unicode.com.br>
 * Colaboração essencial de Alisson Comerlatto e Pedro Woiciechovski
 */
public class Pratica32 {
    public static double densidade(double x, double media, 
            double desvio) {
        double d = (
                        Math.exp(
                            -0.5 * Math.pow(
                                (
                                    (
                                        x-media
                                    ) / desvio
                                ),2
                            )
                        )
                    )
                /(
                    Math.sqrt(
                        2 * Math.PI
                    ) * desvio
                );
        return d;
    }
    
    public static void main(String[] args){
        System.out.println(densidade(-1, 67, 3));
    }
}
